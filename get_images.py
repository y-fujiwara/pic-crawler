from bs4 import BeautifulSoup
import urllib.request
from time import sleep
import yaml
import os
import opencv.face

global_file_num = 0
TARGET_EXT = [".jpg", ".jpeg", ".gif", ".png", ".bmp"]

def get_base_html(base_url, folder_path, file_prefix, queries):
    """
    発火元メソッド
    URLを組み立て対象URLから取得したimgタグの分だけ画像を保存する
    """
    sleep(1)
    global global_file_num
    q_str = "&".join(list(map(lambda x: x + "=" + str(queries[x]), queries)))
    url = base_url + "?" + q_str
    print("=======================================================")
    print("http request for: " + url)
    print("=======================================================")
    html = urllib.request.urlopen(url)
    soup = parse_html(html)


    # 保存先のパスを指定
    base_path = folder_path + "/" + file_prefix
    for s in soup.find_all("img"):
        global_file_num += 1
        path = base_path + "_" + str(global_file_num)
        save_img(s.get("src"), path)

def parse_html(html):
    """
    HTMLオブジェクトをBeautifulSoupオブジェクトに変換する
    """
    soup = BeautifulSoup(html, "html.parser")
    return soup

def save_img(url, file_name):
    """
    img_load()でロードしてきた画像を実際の画像として
    指定フォルダとファイル名で保存する
    """
    # 拡張子取得
    ext = norm_ext(os.path.splitext(url)[1])
    # URL先のイメージをメモリ上にロード
    img = img_load(url)
    fullpath = str(file_name + ext)
    with open(fullpath, "wb") as fout:
        fout.write(img)
    print("=======================================================")
    print("success save image: " + fullpath)
    print("=======================================================")

def img_load(url):
    """
    URLで指定された画像をバイナリ配列としてメモリ上にロードする
    """
    response = urllib.request.urlopen(url).read()
    return response

def norm_ext(ext):
    """
    拡張子を正規化する
    既定の拡張子に対応しない場合はすべてpngとする
    """
    ext = ext.lower()
    if ext in TARGET_EXT:
        return ext
    else:
        # 拡張子がマッチしないときはpngとする
        return ".png"


if __name__ == "__main__":
    print("=======================================================")
    print("setting.yml load start")
    print("=======================================================")
    config = {}
    # 設定ファイルのパスは以下の通り
    with open("config/setting.yml", 'r') as yml:
        config = yaml.load(yml)

    limit = config["config"]["limit"]
    url = config["config"]["url"]
    folder = config["config"]["file"]["output"]
    prefix = config["config"]["file"]["prefix"]

    if not os.path.isdir(folder): 
        os.makedirs(folder)

    for i in range(1, limit, config["config"]["inc"]):
        q = config["config"]["query"]
        target = config["config"]["target"]
        get_base_html(url, folder, prefix, q)
        q[target] = q[target] + config["config"]["inc"]
    
    cascade_path = config["config"]["cv"]["cascade"]
    cut_output = config["config"]["cv"]["output"] + "_" + prefix
    opencv.face.face_cut(folder, cut_output, cascade_path)
