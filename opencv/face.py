import cv2
import os

TARGET_EXT = [".jpg", ".jpeg", ".gif", ".png", ".bmp"]
def face_cut(input_folder, output_folder, cascade_path):
    print("=======================================================")
    print("Start cascade")
    print("=======================================================")
    # 対象フォルダ作成
    if not os.path.isdir(output_folder): 
        os.makedirs(output_folder)

    i = 0
    for line in os.listdir(input_folder):
        line = line.rstrip()
        # パスが対象でなければ処理を飛ばす
        if not (os.path.splitext(line)[1] in TARGET_EXT):
            continue
        # 最後の引数0はグレースケールフラグ -1で自動判定
        image = cv2.imread(input_folder + "/" + line, -1)
        if image is None:
            print('Not open : ',line)
            continue
    
        cascade = cv2.CascadeClassifier(cascade_path)
        facerect = cascade.detectMultiScale(image, scaleFactor=1.2, minNeighbors=1, minSize=(1, 1))
    
        if len(facerect) > 0:
            for rect in facerect:
                # 顔だけ切り出して保存
                # 始点の座標
                x = rect[0]
                y = rect[1]
                # 基準点からの矩形の幅と高さ
                width = rect[2]
                height = rect[3]
                # ここで切り出し
                dst = image[y:y + height, x:x + width]
                save_path = output_folder + '/' + 'image(' + str(i) + ')' + '.png'
                #認識結果の保存
                cv2.imwrite(save_path, cv2.resize(dst, (72, 72), interpolation=cv2.INTER_LINEAR))
                print(line + " to " + save_path + " save!")
                i += 1
    print("Finish")

if __name__ == "__main__":
    pass
