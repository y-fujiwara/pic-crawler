### require

* use anaconda3
  * install conda opencv
  ```.sh
  $ conda config --add channels conda-forge
  $ conda install opencv
  ```

* BeautifulSoup4
  ``` .sh
  $ pip install beautifulsoup4
  ```

* PyYAML
  ``` .sh
  $ pip install pyyaml
  ```
